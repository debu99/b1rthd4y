
# EKS Gitlab CI/CD Pipeline
-----------------------

The aim of this folder is to demo how to deploy an application in AWS EKS cluster and implement GitOps flow on Gitlab.

Before starting
-----------------------
You should install terraform, kubectl, AWS Commandline tools on your machine in order to create an EKS cluster on AWS, a free Gitlab account is required to use Gitlab pipeline and the dockerhub credentials is used to push the image.

Here are the the links :
* https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html for the AWS Cli installation.
* https://v1-13.docs.kubernetes.io/docs/tasks/tools/install-kubectl/ for Kubectl installation
* https://learn.hashicorp.com/terraform/getting-started/install.html for terraform installation
* https://www.gitlab.com
* https://hub.docker.com


Terraform
-----------------------
* settings
Assign the values for variables.tf or use your own customized variable file to override the default values in variables.tf, please refer to README.md in terraform folder for more information
* create EKS cluster
    * initialize and download provider & external modules 
    ```terraform init```
    * Generate and show an execution plan
    ```terraform plan```
    * Create EKS on AWS
    ```terraform apply```

Birthday App
-----------------------
* Birthday app is coded with python3 flask, in order to run it locally, you need to install flask with pip. The app depends on the MongoDB database, you should have the MongoDB ready and set the MONGODB_USERNAME, MONGODB_PASSWORD, MONGODB_HOSTNAME as environment variable for the app to access.
```
cd myapp
pip3 install -r requirements.txt
python3 app.py
```
* To test the app, run 
```
python3 test_app.py
```
* The app is listening on the port 5000 and you can post user date of birth in json format with PUT and GET it with url:
    *  curl -XPUT http://localhost:5000/hello/test -d '{"dateOfBirth":"1999-11-11"}'
    *  curl http://localhost:5000/hello/test

Secret Management
-----------------------
* To avoid saving the database credentials in the git, we use [kubernetes-external-secrets](https://github.com/godaddy/kubernetes-external-secrets "kubernetes-external-secrets") to populate the secrets from SSM
```
helm repo add external-secrets https://godaddy.github.io/kubernetes-external-secrets/
helm install kubernetes-external-secrets external-secrets/kubernetes-external-secrets --set env.AWS_REGION=ap-southeast-1 --namespace kube-system
```

* To set the credentials in AWS SSM
```
aws ssm put-parameter --name "/birthdayapp/MONGODB_USERNAME" --type "String" --value "root" 
aws ssm put-parameter --name "/birthdayapp/MONGODB_PASSWORD" --type "SecureString" --value "mongo_password" 
aws ssm put-parameter --name "/birthdayapp/MONGODB_HOST" --type "String" --value "10.10.12.101" 
```


Gitlab Pipeline
-----------------------
* Create the myapp & myapp_deploy repo in your gitlab account and push the code to your repo
```
cd myapp
git init
git remote add origin git@gitlab.com:<YOUR_ACCOUNT>/myapp.git
git add .
git commit -m "Initial commit"
git push -u origin master
cd ../myapp_deploy
git init
git remote add origin git@gitlab.com:<YOUR_ACCOUNT>/myapp_deploy.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

* Set pipeline variables in order for the two pipeline to work (Goto settings -> CI / CD Settings -> Variables)
  * For repo myapp:
    *  DEPLOYMENT_REPO_NAME : The Deployment Repo name that stores the deployment Helm Chart. E.g.: myapp_deploy
    *  DOCKER_USERNAME & DOCKER_PASSWORD : The dockerhub credential used to push the image.
    *  GITLAB_TOKEN: Get the gitlab_token from Gitlab account -> settings -> access_tokens.

  * For repo myapp_deploy:
    *  AWS_ACCESS_KEY_ID & AWS_SECRET_ACCESS_KEY : The AWS credentials in order to access Kubernetes and deploy myapp.
    *  SERVICE_NAME: The deployment name used to deploy into Kubernetes. E.g.: myapp
    *  KUBE_CONFIG : The base64 encoded Kubectl config file that includes the credentials used to access the kubenetes cluster


GitOps workflow
-----------------------
* Merge your branch code into myapp master
* The .gitlab-ci.yaml pipeline will containerize the application into docker image and push into hub.docker.com
* Then the pipeline will update the image repo and image tag in CUSTOM.yaml file which is located in myapp_deploy repo
* The myapp_deploy code changing will trigger its pipeline which is using helm3 upgrade to install/upgrade the chart into EKS cluster

Tear down EKS
-----------------------
In order to terminate EKS cluster to save money, you can run terraform destroy to delete the infrastructure on AWS
```
terraform destroy
```

Describing project architecture
----------------------------------

* [myapp](./myapp)
    * [.gitlab-ci.yaml](./myapp/.gitlab-ci.yaml) : Gitlab Pipeline code
    * [app.py](./myapp/app.py) : The application code
    * [test_app.py](./myapp/test_app.py) : The application test code
    * [Dockerfile](./myapp/Dockerfile) : Docker file for image containerization
    * [requirements.txt](./myapp/requirements.txt) : Python packages for installation
* [myapp_deploy](./myapp_deploy)
    * [.gitlab-ci.yaml](./myapp_deploy/.gitlab-ci.yaml) : Gitlab Pipeline code
    * [values.yaml](./myapp_deploy/values.yaml) : The default values for helm chart deployment
    * [Chart.yaml](./myapp_deploy/Chart.yaml) : The helm chart versioning file
    * [templates](./myapp_deploy/templates) 
        * [deployment.yaml](./myapp_deploy/template/deployment.yaml) : K8S deployment yaml
        * [service.yaml](./myapp_deploy/template/service.yaml) : K8S ingress yaml
        * [externalsecret.yaml](./myapp_deploy/template/externalsecret.yaml): ExternalSecret yaml
* [terraform](./terraform) : 
    * [README.md](./terraform/README.md) : Readme file for terraform code
    * [variables.tf](./terraform/variables.tf) : Terraform variable file
    * [main.tf](./terraform/main.tf) : Terraform main file
    * [outputs.tf](./terraform/outputs.tf) : Terraform output file
    * [versions.tf](./terraform/versions.tf) :Terraform version file
    * [modules](./terraform/modules) 
        * [bastion](./terraform/modules/bastion) : customerized bastion module
            * [variables.tf](./terraform/modules/bastion/variables.tf) : Bastion module variable file
            * [main.tf](./terraform/modules/bastion/main.tf) : Bastion module main file
            * [outputs.tf](./terraform/modules/bastion/outputs.tf) : Bastion module output file

