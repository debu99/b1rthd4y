from flask import Flask, jsonify
from flask_restful import request, abort, Api, Resource

import pymongo
import datetime
import logging
import json
import os

logging.basicConfig(level=logging.DEBUG)

if 'MONGODB_USERNAME' in os.environ:
    mongo_user = os.environ["MONGODB_USERNAME"]
else:
    mongo_user = ""
if 'MONGODB_PASSWORD' in os.environ:
    mongo_pass = os.environ["MONGODB_PASSWORD"]
else:
    mongo_pass = ""
if 'MONGODB_HOST' in os.environ:
    mongo_host = os.environ["MONGODB_HOST"]
else:
    mongo_host = "127.0.0.1"
if 'MONGODB_PORT' in os.environ:
    mongo_port= os.environ["MONGODB_PORT"]
else:
    mongo_port = "27017"
if 'MONGO_DB' in os.environ:
    mongo_db= os.environ["MONGO_DB"]
else:
    mongo_db = "web"
if 'MONGO_COLLECTION' in os.environ:
    mongo_collection= os.environ["MONGO_COLLECTION"]
else:
    mongo_collection = "web_data"

mongo_url = "mongodb://{}:{}@{}:{}/".format(mongo_user, mongo_pass, mongo_host, mongo_port)


myclient = pymongo.MongoClient(mongo_url, serverSelectionTimeoutMS=3000)
mydb = myclient[mongo_db]
mycol = mydb[mongo_collection]

app = Flask(__name__)
api = Api(app)

def validate_birthday(birthday_string):
    try:
        datetime.datetime.strptime(birthday_string, '%Y-%m-%d')
    except ValueError:
        logging.debug("Incorrect dateOfBirth={} received".format(birthday_string))
        raise ValueError("Incorrect data format, should be YYYY-MM-DD")
        return False
    return True

def update_user_info(username, dateofbirth):
    mycol.find_one_and_update(filter={'username':username}, update={"$set": dateofbirth}, upsert=True)

def check_user(username):
    user_doc = mycol.find_one({'username':username})
    if user_doc is not None:
        logging.debug("check_user return None")
        return True
    else:
        return False

def birthday_msg(username):
    user_doc = mycol.find_one({'username':username})
    message = ''
    logging.debug("user_doc={}".format(user_doc))
    if user_doc is not None and 'dateOfBirth' in user_doc.keys():
        dob_str = user_doc['dateOfBirth']
        dob_obj = datetime.datetime.strptime(dob_str, '%Y-%M-%d').date()
        now_obj = datetime.datetime.utcnow().date()
        new_dob_obj = dob_obj.replace(year=now_obj.year)
        difference = int((now_obj-new_dob_obj).total_seconds()/86400)
        logging.debug("difference={}".format(difference))
        if ((datetime.datetime.utcnow().year % 400 == 0) or (datetime.datetime.utcnow().year % 4 == 0 and datetime.datetime.utcnow().year % 100 != 0)) and (dob_obj.month == 1 or dob_obj.month == 2):
            days_in_year = 366
        else:
            days_in_year = 365
        if difference == 0:
            message = "Hello, " + username + "!  Happy birthday!"
        elif difference < 0:
            message = "Hello, " + username + "! Your birthday is in " + str(abs(difference)) + " day(s)"
        else:
            message = "Hello, " + username + "! Your birthday is in " + str(days_in_year - difference) + " day(s)"
    return app.response_class(response=json.dumps({ "message": message }), status=200, mimetype='application/json')

class birthday(Resource):
    def put(self, username):
        json_data = request.get_json(force=True)
        logging.debug("json_data={}".format(json_data))
        if 'dateOfBirth' in json_data.keys() and validate_birthday(json_data['dateOfBirth']):
            update_user_info(username, json_data)
            return "No Content", 204
        else:
            abort(500, message="dateOfBirth must be YYYY-MM-DD")

    def get(self, username):
        if(check_user(username)):
            return birthday_msg(username)
        else:
            abort(404, message="user not found")

class help(Resource):
    def get(self):
        abort(404, message="Error: please send /hello/<username>")

class ping(Resource):
    def get(self):
            return app.response_class("pong", status=200)

class healthcheck(Resource):
    def get(self):
        try:
            mydb.command('ping')
        except Exception as e:
            logging.error("Exception={}".format(str(e)))
            abort(500, message="Error")
        else:
            #return app.response_class(response=json.dumps({"message":"OK"}), status=200, mimetype='application/json')
            return app.response_class("OK", status=200)
            

api.add_resource(help, '/', '/hello', '/hello/')
api.add_resource(ping, '/ping')
api.add_resource(healthcheck, '/healthcheck', '/healthz')

api.add_resource(birthday, '/hello/<string:username>')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

