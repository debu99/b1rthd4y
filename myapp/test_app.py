import unittest
import app as myapp
import json
import random, string
import time, datetime


help_return = '{"message": "Error: please send /hello/<username>"}'
user_non_exist_return = '{"message": "user not found"}'

random_user1 = ''.join(random.choice(string.ascii_lowercase) for i in range(7))
random_user2 = ''.join(random.choice(string.ascii_lowercase) for i in range(7))
random_user3 = ''.join(random.choice(string.ascii_lowercase) for i in range(7))

random_user1_params = {'dateOfBirth': (datetime.datetime.today() - datetime.timedelta(days=-1)).strftime('%Y-%m-%d')}
random_user2_params = {'dateOfBirth': (datetime.datetime.today() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')}
random_user3_params = {'dateOfBirth': datetime.datetime.today().strftime('%Y-%m-%d')}

def return_message(username, dateofbirth_days):
    if dateofbirth_days == 0:
        return '{"message": "Hello, ' + username + '!  Happy birthday!"}'
    else:
        return '{"message": "Hello, ' + username + '! Your birthday is in ' + str(dateofbirth_days) + ' day(s)"}'

class TestApp(unittest.TestCase):

    def setUp(self):
        myapp.app.testing = True
        self.app = myapp.app.test_client()

    def test_index(self):
        respdata = self.app.get("/")
        self.assertEqual(404, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(help_return))

    def test_username(self):
        respdata = self.app.get("/hello")
        self.assertEqual(404, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(help_return))

    def test_username_dir(self):
        respdata = self.app.get("/hello/")
        self.assertEqual(404, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(help_return))

    def ping(self):
        respdata = self.app.get("/healthz")
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(respdata.data.decode("utf-8")  == "pong")

    def test_healthcheck(self):
        respdata = self.app.get("/healthcheck")
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(respdata.data.decode("utf-8")  == "OK")

    def test_healthz(self):
        respdata = self.app.get("/healthz")
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(respdata.data.decode("utf-8")  == "OK")
    '''
    def test_user1(self):
        respdata = self.app.get("/hello/test1")
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(return_message("test1", 1)))


    def test_user2(self):
        respdata = self.app.get("/hello/test2")
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(return_message("test2", 365)))


    def test_user3(self):
        respdata = self.app.get("/hello/test3")
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(return_message("test3", 0)))
    '''

    def test_user_non_exist(self):
        respdata = self.app.get("/hello/user_non_exist")
        self.assertEqual(404, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(user_non_exist_return))

    def test_put_new_user1(self):
        respdata = self.app.put("/hello/" + random_user1, data=json.dumps(random_user1_params), content_type='application/json')
        self.assertEqual(204, respdata.status_code)

        time.sleep(1)
        respdata = self.app.get("/hello/" + random_user1)
        print("{} - {} - {}".format(random_user1, respdata, respdata.data))
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(return_message(random_user1, 1)))


    def test_put_new_user2(self):
        respdata = self.app.put("/hello/" + random_user2, data=json.dumps(random_user2_params), content_type='application/json')
        self.assertEqual(204, respdata.status_code)

        time.sleep(1)
        respdata = self.app.get("/hello/" + random_user2)
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(return_message(random_user2, 365)))

    def test_put_new_user3(self):
        respdata = self.app.put("/hello/" + random_user3, data=json.dumps(random_user3_params), content_type='application/json')
        self.assertEqual(204, respdata.status_code)

        time.sleep(1)
        respdata = self.app.get("/hello/" + random_user3)
        self.assertEqual(200, respdata.status_code)
        self.assertTrue(json.loads(respdata.data) == json.loads(return_message(random_user3, 0)))

if __name__ == '__main__':
    unittest.main()
