## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| cluster\_instance\_type |  | string | `"m3.medium"` | no |
| cluster\_name | Name of the EKS cluster. Also used as a prefix in names of related resources. | string | `"eks_test"` | no |
| cluster\_node\_number |  | string | `"1"` | no |
| cluster\_version | Kubernetes version to use for the EKS cluster. | string | `"1.14"` | no |
| environment | The environment tag. | string | `"test"` | no |
| map\_accounts | Additional AWS account numbers to add to the aws-auth configmap. | list(string) | `[ "702051619253" ]` | no |
| map\_users | Additional IAM users to add to the aws-auth configmap. | object | `[ { "groups": [ "system:masters" ], "userarn": "arn:aws:iam::702051619253:user/vincent", "username": "vincent" } ]` | no |
| private\_subnets | A list of private subnets inside the VPC | list | `[ "10.10.1.0/24", "10.10.2.0/24" ]` | no |
| profile | The AWS profile name. | string | `"eks"` | no |
| public\_subnets | A list of public subnets inside the VPC. | list | `[ "10.10.11.0/24", "10.10.12.0/24" ]` | no |
| region | The AWS region. | string | `"ap-southeast-1"` | no |
| ssh\_public\_key | The SSH public key for EC2 instance. | string | `"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCF97NzG/+4ENXHIe73NmY12kc1k5S7re7cwrD+Rulz7Cvd03DljRjh1mfyJnhhR9tihyM/To9gJRM1cO8wpbwXAlrtQo4h0voaabDF5eMzr4C1yxqWf+Tq3kXjIsKOenq844hWRJFd7KeOhVhhzPsDWb0OU1ztWLaRbw4JYXVfnPjxcGUaEh0O/ygmyONcoARh9cOeKGMKAjgMFCVsH1wqV03csWH6KsrVW3OEU+B7cWXu5OKCWIG2QEQ4JqDyIBfMBw2iq9MAGoDE9kHv0aSGkvaH3XfKAGA40lAcCOXG8bs32iTIDlV/bMhd+WnNev4QXbq6MJVrQ+cyQn94v/WRhE7v8FpBuh9zsT4DWwpjf7JFUzLdfPCRVYcjGXuM7095CS29RDqv6Uh0UUYRZAtX2vP2xmIpASAdWO1h6JapmriwRdwShqi+/nBuRM8evYklfOsyAl2B9Su/N9GnuWNHJbjwh3Wck2PBBLObp4aWyt44wWtZ6rJEzQ7hPtUEiwZA9Xg0mO4I5FrIqfXHly6oY4f9vgXZ0Bmf93d5chs7pVyevJBJN41zSRgmnTabex7j2QqcdmYEvVzODXPrHCVdsP+kd2omFVVrT417uZTjtrfUAu+Wn8RM9If7CNBsoBROeowAtKTWxRu7SuKqjOKetUAwAdemeyE1FZq4sPJuw=="` | no |
| stage | The stage tag. | string | `"dev"` | no |
| vpc\_azs | A list of availability zones in the region | list | `[ "ap-southeast-1a", "ap-southeast-1b" ]` | no |
| vpc\_cidr | The CIDR block for the VPC. | string | `"10.10.0.0/16"` | no |
| vpc\_name | Name to be used on all the resources as identifier. | string | `"dev"` | no |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_arn | The Amazon Resource Name \(ARN\) of the cluster. |
| cluster\_certificate\_authority\_data | Nested attribute containing certificate-authority-data for your cluster. This is the base64 encoded certificate data required to communicate with your cluster. |
| cluster\_endpoint | The endpoint for your EKS Kubernetes API. |
| cluster\_id | The name/id of the EKS cluster. |

