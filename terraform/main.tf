provider "aws" {
  profile = var.profile
  region  = var.region
}

data "aws_caller_identity" "current" {}

module "vpc" {
  source          = "terraform-aws-modules/vpc/aws"
  name            = var.vpc_name
  cidr            = var.vpc_cidr
  azs             = var.vpc_azs
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = {
    Stage                                       = var.stage
    Environment                                 = var.environment
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

module "bastion" {
  source             = "./modules/bastion"
  environment        = var.environment
  stage              = var.stage
  vpc_public_subnets = module.vpc.public_subnets
  vpc_id             = module.vpc.vpc_id
  ssh_public_key     = var.ssh_public_key
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = var.cluster_name
  cluster_version = var.cluster_version
  subnets         = module.vpc.private_subnets
  vpc_id          = module.vpc.vpc_id

  map_users    = var.map_users
  map_accounts = var.map_accounts

  write_kubeconfig = true
  manage_aws_auth  = true

  kubeconfig_aws_authenticator_env_variables = {
    AWS_PROFILE = var.profile
  }
  workers_group_defaults = {
    key_name = "bastion_key"
  }

  worker_groups = [
    {
      instance_type                 = var.cluster_instance_type
      asg_desired_capacity          = var.cluster_node_number
      autoscaling_enabled           = true
      additional_security_group_ids = [module.bastion.bastion_sg_id]
    }
  ]

  tags = {
    Stage       = var.stage
    Environment = var.environment
    Name        = var.cluster_name
  }
}


resource "aws_iam_policy" "eks-ssm" {
  name = "eks-ssm"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ssm:GetParameter"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ssm-policy-attach" {
  role       = module.eks.worker_iam_role_name
  policy_arn = aws_iam_policy.eks-ssm.arn
}
