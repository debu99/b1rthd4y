
variable "vpc_id" {
  default = ""
}

variable "vpc_public_subnets" {
  default = []
}

variable "environment" {
  default = ""
}

variable "stage" {
  default = ""
}

variable "ssh_public_key" {
  default = ""
}