variable "region" {
  description = "The AWS region."
  default     = "ap-southeast-1"
}

variable "profile" {
  description = "The AWS profile name."
  default     = "eks"
}

variable "vpc_name" {
  description = "Name to be used on all the resources as identifier."
  default     = "dev"
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC."
  default     = "10.10.0.0/16"
}

variable "vpc_azs" {
  description = "A list of availability zones in the region"
  default     = ["ap-southeast-1a", "ap-southeast-1b"]
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  default     = ["10.10.1.0/24", "10.10.2.0/24"]
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC."
  default     = ["10.10.11.0/24", "10.10.12.0/24"]
}

variable "environment" {
  description = "The environment tag."
  default     = "test"
}

variable "stage" {
  description = "The stage tag."
  default     = "dev"
}

variable "ssh_public_key" {
  description = "The SSH public key for EC2 instance."
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCF97NzG/+4ENXHIe73NmY12kc1k5S7re7cwrD+Rulz7Cvd03DljRjh1mfyJnhhR9tihyM/To9gJRM1cO8wpbwXAlrtQo4h0voaabDF5eMzr4C1yxqWf+Tq3kXjIsKOenq844hWRJFd7KeOhVhhzPsDWb0OU1ztWLaRbw4JYXVfnPjxcGUaEh0O/ygmyONcoARh9cOeKGMKAjgMFCVsH1wqV03csWH6KsrVW3OEU+B7cWXu5OKCWIG2QEQ4JqDyIBfMBw2iq9MAGoDE9kHv0aSGkvaH3XfKAGA40lAcCOXG8bs32iTIDlV/bMhd+WnNev4QXbq6MJVrQ+cyQn94v/WRhE7v8FpBuh9zsT4DWwpjf7JFUzLdfPCRVYcjGXuM7095CS29RDqv6Uh0UUYRZAtX2vP2xmIpASAdWO1h6JapmriwRdwShqi+/nBuRM8evYklfOsyAl2B9Su/N9GnuWNHJbjwh3Wck2PBBLObp4aWyt44wWtZ6rJEzQ7hPtUEiwZA9Xg0mO4I5FrIqfXHly6oY4f9vgXZ0Bmf93d5chs7pVyevJBJN41zSRgmnTabex7j2QqcdmYEvVzODXPrHCVdsP+kd2omFVVrT417uZTjtrfUAu+Wn8RM9If7CNBsoBROeowAtKTWxRu7SuKqjOKetUAwAdemeyE1FZq4sPJuw=="
}


variable "cluster_name" {
  description = "Name of the EKS cluster. Also used as a prefix in names of related resources."
  default     = "eks_test"
}

variable "cluster_version" {
  description = "Kubernetes version to use for the EKS cluster."
  default     = "1.14"
}

variable "cluster_instance_type" {
  default = "m3.medium"
}

variable "cluster_node_number" {
  default = "1"
}


variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "702051619253",
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::702051619253:user/vincent"
      username = "vincent"
      groups   = ["system:masters"]
    },
  ]
}
